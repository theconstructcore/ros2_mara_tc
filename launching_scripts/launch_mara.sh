#! /bin/bash 

# [2020-08-14] The sim fails if gzclient is launched. Remember to disable/override gzclient when running this script. Use gzweb instead.

source /usr/share/gazebo-9/setup.sh
source /opt/ros/dashing/setup.bash
source /home/simulations/ros2_sims_ws/install/setup.bash


ros2 launch mara_gazebo mara.launch.py

