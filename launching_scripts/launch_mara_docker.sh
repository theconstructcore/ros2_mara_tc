#! /bin/bash 

# Source where the simulation files will be
MARA_SIM_PATH=/home/user/ros2_mara_ws/install/setup.bash
source $MARA_SIM_PATH
ros2 launch mara_gazebo mara.launch.py
